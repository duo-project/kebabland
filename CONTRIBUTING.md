# Contribuer au projet

## Architecture du projet
### Assets 
Contient les variables globales du projet

### Components
Contient les différents composents du site :
- [ ] Button : Bouton réutilisable, on lui passe un titre et une action
- [ ] Card : Carte réutilisable, on lui passe une image et un titre
- [ ] Error404 : L'écran d'erreur si on prends une route non authorisée
- [ ] Header : Contient le logo du site
- [ ] KebabSystem : Contient la logique de composition de kebab du site 
- [ ] Layout : Permet d'avoir un padding prédéfini 
- [ ] ModalCart : Le panier, contient les kebabs ainsi que leurs quantités 
- [ ] Options : Contient les options pour les viandes, pains, sauces et garnitures
- [ ] Suggestions : Contient les kebabs prédéfinis
- [ ] Payment : Contient la page de paiement du panier

### Images
Contient les images du site

### Pages 
Contient la page principale et la page d'erreur

### Utils
Contient les fonctions de recherche