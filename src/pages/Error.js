import React from "react";
import Layout from "../components/Layout/Layout.jsx";
import Error404 from "../components/Error404/Error404";

const Error = () => {
  return (
    <>
      <Layout>
        <Error404 />
      </Layout>
    </>
  );
};

export default Error;
