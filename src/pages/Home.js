import React from "react";
import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import KebabSystem from "../components/KebabSystem/KebabSystem";
import Layout from "../components/Layout/Layout.jsx";

const Home = () => {
  return (
    <>
      <Header />
      <Layout>
        <KebabSystem />
      </Layout>
      <Footer />
    </>
  );
};

export default Home;
