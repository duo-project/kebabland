export const findGarn = (state, val) => {
  return state.garniture.find((garn) => garn === val);
};

export const findSauce = (state, val) => {
  return state.sauces.find((sauce) => sauce === val);
};
