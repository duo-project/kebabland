import React from "react";
import "../Footer/Footer.scss";
import footer_img from "../../images/footer.png";

const Footer = () => {
  return (
    <div>
      <img className="footer_img" src={footer_img} alt="footer img" />
    </div>
  );
};

export default Footer;
