import React from "react";
import "./Button.scss";

const ButtonCommand = ({ parentAction }) => {
  return (
    <div className="btn-commander" onClick={parentAction}>
      Passer la commande
    </div>
  );
};

export default ButtonCommand;
