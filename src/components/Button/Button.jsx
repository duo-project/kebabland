import React from "react";
import "./Button.scss";

const Button = ({ parentAction, title }) => {
  return (
    <div className="btn-continue" onClick={parentAction}>
      {title}
    </div>
  );
};

export default Button;
