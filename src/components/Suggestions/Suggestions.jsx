import React from "react";
import Button from "../Button/Button";
import "./Suggestions.scss";

const Suggestions = ({ addClassique, addVege, addBBQ, personalise }) => {
  return (
    <div className="suggestions">
      <Button title="Le classique" parentAction={addClassique} />
      <Button title="Le vege" parentAction={addVege} />
      <Button title="Le bbq" parentAction={addBBQ} />
      <Button title="Personnaliser" parentAction={personalise} />
    </div>
  );
};

export default Suggestions;
