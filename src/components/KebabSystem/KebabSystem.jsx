import { Component } from "react";
import Button from "../Button/Button";
import Card from "../Card/Card";
import GarnitureOptions from "../Options/GarnitureOptions/GarnitureOptions";
import PainOptions from "../Options/PainOptions/PainOptions";
import Recap from "../Options/Recap";
import SaucesOptions from "../Options/SaucesOptions/SaucesOptions";
import ViandeOptions from "../Options/ViandeOptions/ViandeOption";
import PainImg from "../../images/kebab.png";
import GaletteImg from "../../images/galette.png";
import BaguetteImg from "../../images/baguette.png";
import ViandeImg from "../../images/viande.png";
import TofuImg from "../../images/tofu.png";
import EasterEgg from "../../images/easteregg.png";
import "./KebabSystem.scss";
import LargeCard from "../Card/LargeCard";
import ModalCart from "../ModalCart/ModalCart";
import { findGarn, findSauce } from "../../utils/util";
import Suggestions from "../Suggestions/Suggestions";
import Order from "../Order/Order";
import Aos from "aos";
import "aos/dist/aos.css";

export default class KebabSystem extends Component {
  constructor() {
    super();
    this.state = {
      kebab: {
        pain: "",
        viande: "",
        sauces: [],
        garniture: [],
        quantity: 0,
      },
      pain: "",
      viande: "",
      sauces: [],
      saucesCount: 0,
      garniture: [],
      store: [],
      price: 0,
      modal: false,
      quantity: 0,
      mode: "suggestions",
      easterEgg: 0,
    };
  }
  componentDidMount() {
    Aos.init({ duration: 1000 });
  }

  render() {
    const addValues = () => {
      let updatedKebab = {
        pain: this.state.pain,
        viande: this.state.viande,
        sauces: this.state.sauces,
        garniture: this.state.garniture,
        quantity: this.state.quantity,
      };

      this.setState({ kebab: updatedKebab, quantity: 0 });
    };

    const addQuantity = () => {
      let k;
      let bool = false;
      this.state.store.map((keb) => {
        k = this.state.store.find((keb2) => {
          return (
            keb2.pain === this.state.pain &&
            keb2.viande === this.state.viande &&
            keb2.sauces.join() === this.state.sauces.join() &&
            keb2.garniture.join() === this.state.garniture.join()
          );
        });
        k && !bool && (k.quantity = k.quantity + 1);
        k && (bool = true);
        return null;
      });
    };

    const leClassique = () => {
      this.setState({
        pain: "pain",
        viande: "viande",
        sauces: ["blanche"],
        saucesCount: 1,
        garniture: ["salad", "tomatoes", "oignons"],
        quantity: this.state.quantity + 1,
        mode: "recap",
        easterEgg: this.state.easterEgg + 1,
      });
      addValues();
    };
    const leVege = () => {
      this.setState({
        pain: "pain",
        viande: "tofu",
        sauces: ["blanche"],
        garniture: [],
        quantity: this.state.quantity + 1,
        mode: "recap",
        easterEgg: this.state.easterEgg + 1,
      });
      addValues();
    };
    const leBBQ = () => {
      this.setState({
        pain: "pain",
        viande: "viande",
        sauces: ["bbq"],
        saucesCount: 1,
        garniture: ["salad", "tomatoes", "oignons"],
        quantity: this.state.quantity + 1,
        mode: "recap",
        easterEgg: this.state.easterEgg + 1,
      });
      addValues();
    };
    const addKebab = () => {
      let k = this.state.store.find((keb) => {
        return (
          keb.pain === this.state.pain &&
          keb.viande === this.state.viande &&
          keb.sauces.join() === this.state.sauces.join() &&
          keb.garniture.join() === this.state.garniture.join()
        );
      });
      console.log(k);
      if (!k) {
        this.setState({
          quantity: 1,
          store: [...this.state.store, this.state.kebab],
          price: this.state.price + 5.5,
        });
      } else {
        addQuantity(this.state.kebab);
      }
    };

    const resetKebab = () => {
      this.setState({
        kebab: [
          {
            pain: "",
            viande: "",
            sauces: [],
            garniture: [],
            quantity: 0,
          },
        ],
        pain: "",
        viande: "",
        garniture: [],
        sauces: [],
        saucesCount: 0,
        quantity: 0,
      });
    };
    const rmKebab = (kebab) => {
      const newstore = this.state.store.filter((el) => el !== kebab);
      this.setState({ store: newstore, quantity: this.state.quantity - 1 });
      addValues();
      console.log(this.state.store);
    };
    const addSauces = (sauce) => {
      if (this.state.saucesCount < 2) {
        this.setState({
          sauces: [...this.state.sauces, sauce],
          saucesCount: this.state.saucesCount + 1,
        });
      }
    };
    const rmSauces = (sauce) => {
      if (this.state.saucesCount >= 0) {
        const slicedData = this.state.sauces.filter((data) => data !== sauce);
        this.setState({
          sauces: slicedData,
          saucesCount: this.state.saucesCount - 1,
        });
      }
    };
    const rmGarn = (garn) => {
      if (this.state.garniture.find((garn) => garn)) {
        const slicedData = this.state.garniture.filter((data) => data !== garn);
        this.setState({
          garniture: slicedData,
        });
      }
    };

    return (
      <>
        <div className="container" data-aos="flip-right">
          {this.state.mode === "suggestions" && (
            <>
              {this.state.mode === "pain" && (
                <h3>Quelques suggestions pour vous.</h3>
              )}
              <Suggestions
                personalise={() => this.setState({ mode: "pain" })}
                addClassique={leClassique}
                addVege={leVege}
                addBBQ={leBBQ}
              />
            </>
          )}
          {this.state.mode === "pain" && (
            <div data-aos="fade-left">
              {this.state.mode === "pain" && (
                <h3>Pain, Baguette ou Galette ?</h3>
              )}
              <PainOptions
                addPain={() => this.setState({ pain: "pain" })}
                addGalette={() => this.setState({ pain: "galette" })}
                addBaguette={() => this.setState({ pain: "baguette" })}
                value={this.state.pain}
              />
              <div className="flex-grid">
                <Button
                  title="Retour"
                  parentAction={() => {
                    this.setState({ mode: "suggestions" });
                  }}
                />
                <Button
                  title="Continuer"
                  parentAction={() => {
                    this.setState({ mode: "viande" });
                  }}
                />
              </div>
            </div>
          )}
          {this.state.mode === "viande" && (
            <div data-aos="fade-left">
              {this.state.mode === "viande" && <h3>Viande ou Tofu ?</h3>}
              <ViandeOptions
                addViande={() => this.setState({ viande: "viande" })}
                addTofu={() => this.setState({ viande: "tofu" })}
                value={this.state.viande}
              />
              <div className="flex-grid">
                <Button
                  title="Retour"
                  parentAction={() => {
                    this.setState({ mode: "pain" });
                  }}
                />
                <Button
                  title="Continuer"
                  parentAction={() => {
                    this.setState({ mode: "garnitures" });
                  }}
                />
              </div>
            </div>
          )}
          {this.state.mode === "garnitures" && (
            <div data-aos="fade-left">
              {this.state.mode === "garnitures" && (
                <h3>Salade, Tomates, Oignons ?</h3>
              )}
              <GarnitureOptions
                addSalad={() =>
                  this.setState({
                    garniture: [...this.state.garniture, "salad"],
                  })
                }
                addTomato={() =>
                  this.setState({
                    garniture: [...this.state.garniture, "tomatoes"],
                  })
                }
                addOignons={() =>
                  this.setState({
                    garniture: [...this.state.garniture, "oignons"],
                  })
                }
                rmSalad={() => rmGarn("salad")}
                rmTomato={() => rmGarn("tomatoes")}
                rmOignons={() => rmGarn("oignons")}
                value={this.state.garniture}
              />
              <div className="flex-grid">
                <Button
                  title="Retour"
                  parentAction={() => {
                    this.setState({ mode: "viande" });
                  }}
                />
                <Button
                  title="Continuer"
                  parentAction={() => {
                    this.setState({ mode: "sauces" });
                  }}
                />
              </div>
            </div>
          )}
          {this.state.mode === "sauces" && (
            <div data-aos="fade-left">
              <br />
              <br />
              {this.state.mode === "sauces" && <h3>Sauces ?</h3>}
              <SaucesOptions
                addBlanche={() => addSauces("blanche")}
                addHarissa={() => addSauces("harissa")}
                addAndalouse={() => addSauces("andalouse")}
                addBBQ={() => addSauces("bbq")}
                addKetchup={() => addSauces("ketchup")}
                addCurry={() => addSauces("curry")}
                rmBlanche={() => rmSauces("blanche")}
                rmHarissa={() => rmSauces("harissa")}
                rmAndalouse={() => rmSauces("andalouse")}
                rmBBQ={() => rmSauces("bbq")}
                rmKetchup={() => rmSauces("ketchup")}
                rmCurry={() => rmSauces("curry")}
                value={this.state.sauces}
              />
              <div className="flex-grid">
                <Button
                  title="Retour"
                  parentAction={() => {
                    this.setState({ mode: "garnitures" });
                  }}
                />
                <Button
                  title="Continuer"
                  parentAction={() => {
                    this.setState({ mode: "recap" });
                  }}
                />
              </div>
            </div>
          )}

          {this.state.mode === "recap" && (
            <div data-aos="fade-left">
              <br />
              <br />
              {this.state.mode === "recap" && <h3>On récapitule</h3>}
              <Recap>
                {/* pain */}
                {this.state.pain === "pain" && (
                  <Card
                    option2={() => this.setState({ mode: "pain" })}
                    image={PainImg}
                    title="Pain"
                  />
                )}
                {this.state.pain === "galette" && (
                  <Card
                    option2={() => this.setState({ mode: "pain" })}
                    image={GaletteImg}
                    title="Galette"
                  />
                )}
                {this.state.pain === "baguette" && (
                  <Card
                    option2={() => this.setState({ mode: "pain" })}
                    image={BaguetteImg}
                    title="Baguette"
                  />
                )}
                {/* viande */}
                <div className="plus">+</div>
                {this.state.viande === "viande" && (
                  <Card
                    option2={() => this.setState({ mode: "viande" })}
                    image={ViandeImg}
                    title="Viande"
                  />
                )}
                {this.state.viande === "tofu" && (
                  <Card
                    option2={() => this.setState({ mode: "viande" })}
                    image={TofuImg}
                    title="Tofu"
                  />
                )}
                <div className="plus">+</div>
                {/* garniture */}
                {findGarn(this.state, "salad") &&
                  findGarn(this.state, "tomatoes") &&
                  findGarn(this.state, "oignons") && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "garnitures" })}
                      val="Salade & Tomate & Oignons"
                    />
                  )}
                {findGarn(this.state, "salad") &&
                  findGarn(this.state, "tomatoes") &&
                  !findGarn(this.state, "oignons") && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "garnitures" })}
                      val="Salade & Tomate"
                    />
                  )}
                {findGarn(this.state, "salad") &&
                  !findGarn(this.state, "tomatoes") &&
                  findGarn(this.state, "oignons") && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "garnitures" })}
                      val="Salade & Oignons"
                    />
                  )}
                {!findGarn(this.state, "salad") &&
                  findGarn(this.state, "tomatoes") &&
                  findGarn(this.state, "oignons") && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "garnitures" })}
                      val="Oignons & Tomate"
                    />
                  )}
                {!findGarn(this.state, "salad") &&
                  !findGarn(this.state, "tomatoes") &&
                  findGarn(this.state, "oignons") && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "garnitures" })}
                      val="Oignons"
                    />
                  )}
                {findGarn(this.state, "salad") &&
                  !findGarn(this.state, "tomatoes") &&
                  !findGarn(this.state, "oignons") && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "garnitures" })}
                      val="Salade"
                    />
                  )}
                {!findGarn(this.state, "salad") &&
                  findGarn(this.state, "tomatoes") &&
                  !findGarn(this.state, "oignons") && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Tomate"
                    />
                  )}
                {!findGarn(this.state, "salad") &&
                  !findGarn(this.state, "tomatoes") &&
                  !findGarn(this.state, "oignons") && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "garnitures" })}
                      val="empty "
                    />
                  )}
                {/* sauces */}
                <div className="plus">+</div>
                {findSauce(this.state, "blanche") &&
                  findSauce(this.state, "harissa") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Blanche & Harissa"
                    />
                  )}
                {findSauce(this.state, "blanche") &&
                  findSauce(this.state, "andalouse") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Blanche & Andalouse"
                    />
                  )}
                {findSauce(this.state, "blanche") &&
                  findSauce(this.state, "bbq") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Blanche & BBQ"
                    />
                  )}
                {findSauce(this.state, "blanche") &&
                  findSauce(this.state, "ketchup") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Blanche & Ketchup"
                    />
                  )}
                {findSauce(this.state, "blanche") &&
                  findSauce(this.state, "curry") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Blanche & Curry"
                    />
                  )}
                {findSauce(this.state, "harissa") &&
                  findSauce(this.state, "andalouse") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Harissa & Andalouse"
                    />
                  )}
                {findSauce(this.state, "harissa") &&
                  findSauce(this.state, "bbq") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Harissa & BBQ"
                    />
                  )}
                {findSauce(this.state, "harissa") &&
                  findSauce(this.state, "ketchup") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Harissa & Ketchup"
                    />
                  )}
                {findSauce(this.state, "harissa") &&
                  findSauce(this.state, "curry") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Harissa & Curry"
                    />
                  )}
                {findSauce(this.state, "andalouse") &&
                  findSauce(this.state, "bbq") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Andalouse & BBQ"
                    />
                  )}
                {findSauce(this.state, "andalouse") &&
                  findSauce(this.state, "ketchup") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Andalouse & Ketchup"
                    />
                  )}
                {findSauce(this.state, "andalouse") &&
                  findSauce(this.state, "curry") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Andalouse & Curry"
                    />
                  )}
                {findSauce(this.state, "bbq") &&
                  findSauce(this.state, "ketchup") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="BBQ & Ketchup"
                    />
                  )}
                {findSauce(this.state, "bbq") &&
                  findSauce(this.state, "curry") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="BBQ & Curry"
                    />
                  )}
                {findSauce(this.state, "ketchup") &&
                  findSauce(this.state, "curry") &&
                  this.state.saucesCount <= 2 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Ketchup & Curry"
                    />
                  )}
                {findSauce(this.state, "blanche") &&
                  this.state.saucesCount === 1 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Blanche"
                    />
                  )}
                {findSauce(this.state, "harissa") &&
                  this.state.saucesCount === 1 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Harissa"
                    />
                  )}
                {findSauce(this.state, "andalouse") &&
                  this.state.saucesCount === 1 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Andalouse"
                    />
                  )}
                {findSauce(this.state, "bbq") &&
                  this.state.saucesCount === 1 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="BBQ"
                    />
                  )}
                {findSauce(this.state, "ketchup") &&
                  this.state.saucesCount === 1 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Ketchup"
                    />
                  )}
                {findSauce(this.state, "curry") &&
                  this.state.saucesCount === 1 && (
                    <LargeCard
                      parentAction={() => this.setState({ mode: "sauces" })}
                      val="Curry"
                    />
                  )}
                {this.state.saucesCount === 0 && (
                  <LargeCard
                    parentAction={() => this.setState({ mode: "sauces" })}
                    val="empty"
                  />
                )}
              </Recap>

              <div className="flex-grid">
                <Button
                  title="Annuler"
                  parentAction={() => {
                    resetKebab();
                    this.setState({ mode: "pain" });
                  }}
                />
                <Button
                  title="Commander"
                  parentAction={async () => {
                    await this.setState({
                      mode: "pain",
                      modal: true,
                      quantity: this.state.quantity + 1,
                    });
                    await addValues();
                    addKebab();
                    resetKebab();
                  }}
                />
              </div>
            </div>
          )}

          {this.state.mode === "paiement" && <Order parentAction={()=>{this.setState({mode: 'suggestions'})}} />}
        </div>
        {this.state.modal && (
          <ModalCart
            price={this.state.price}
            store={this.state.store}
            removeKebab={rmKebab}
            commandAction={() =>
              this.setState({ mode: "paiement", modal: false })
            }
          />
        )}
        {this.state.easterEgg === 3 && (
          <Card
            title="Easter egg"
            image={EasterEgg}
            parentAction={() => this.setState({ easterEgg: 0 })}
          />
        )}
      </>
    );
  }
}
