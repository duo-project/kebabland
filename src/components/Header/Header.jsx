import React from "react";
import "../Header/Header.scss";
import Header_img from "../../images/logo.png";

const Header = () => {
  return (
    <>
      <div className="global_page">
        <img className="Header_img" src={Header_img} alt="header img" />
      </div>
    </>
  );
};

export default Header;
