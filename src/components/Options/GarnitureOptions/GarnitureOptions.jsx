import React from "react";
import Card from "../../Card/Card";
import "../Options.scss";
import Salad from "../../../images/salade.png";
import Tomato from "../../../images/tomate.png";
import Oignons from "../../../images/oignon.png";

const GarnitureOptions = ({
  addSalad,
  addTomato,
  addOignons,
  value,
  rmOignons,
  rmSalad,
  rmTomato,
}) => {
  return (
    <div className="grid-choice">
      {value.find((data) => data === "salad") ? (
        <Card
          title="Salad"
          image={Salad}
          parentAction={rmSalad}
          valid="valid"
        />
      ) : (
        <Card title="Salad" image={Salad} parentAction={addSalad} />
      )}
      {value.find((data) => data === "tomatoes") ? (
        <Card
          title="Tomatoes"
          image={Tomato}
          parentAction={rmTomato}
          valid="valid"
        />
      ) : (
        <Card title="Tomatoes" image={Tomato} parentAction={addTomato} />
      )}
      {value.find((data) => data === "oignons") ? (
        <Card
          title="Oignons"
          image={Oignons}
          parentAction={rmOignons}
          valid="valid"
        />
      ) : (
        <Card title="Oignons" image={Oignons} parentAction={addOignons} />
      )}
    </div>
  );
};

export default GarnitureOptions;
