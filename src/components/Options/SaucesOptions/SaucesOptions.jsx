import React from "react";
import Card from "../../Card/Card";
import "../Options.scss";
import Blanche from "../../../images/blache.png";
import Harissa from "../../../images/harissa.png";
import Andalouse from "../../../images/andalouse.png";
import BBQ from "../../../images/bbq.png";
import Ketchup from "../../../images/ketchup.png";
import Curry from "../../../images/curry.png";

const SaucesOptions = ({
  addBlanche,
  addHarissa,
  addAndalouse,
  addBBQ,
  addKetchup,
  addCurry,
  value,
  rmBlanche,
  rmHarissa,
  rmAndalouse,
  rmBBQ,
  rmKetchup,
  rmCurry,
}) => {
  return (
    <div className="grid-choice">
      {value.find((data) => data === "blanche") ? (
        <Card
          title="Blanche"
          image={Blanche}
          parentAction={rmBlanche}
          valid="valid"
        />
      ) : (
        <Card title="Blanche" image={Blanche} parentAction={addBlanche} />
      )}
      {value.find((data) => data === "harissa") ? (
        <Card
          title="Harissa"
          image={Harissa}
          parentAction={rmHarissa}
          valid="valid"
        />
      ) : (
        <Card title="Harissa" image={Harissa} parentAction={addHarissa} />
      )}
      {value.find((data) => data === "andalouse") ? (
        <Card
          title="Andalouse"
          image={Andalouse}
          parentAction={rmAndalouse}
          valid="valid"
        />
      ) : (
        <Card title="Andalouse" image={Andalouse} parentAction={addAndalouse} />
      )}
      {value.find((data) => data === "bbq") ? (
        <Card title="BBQ" image={BBQ} parentAction={rmBBQ} valid="valid" />
      ) : (
        <Card title="BBQ" image={BBQ} parentAction={addBBQ} />
      )}
      {value.find((data) => data === "ketchup") ? (
        <Card
          title="Ketchup"
          image={Ketchup}
          parentAction={rmKetchup}
          valid="valid"
        />
      ) : (
        <Card title="Ketchup" image={Ketchup} parentAction={addKetchup} />
      )}
      {value.find((data) => data === "curry") ? (
        <Card
          title="Curry"
          image={Curry}
          parentAction={rmCurry}
          valid="valid"
        />
      ) : (
        <Card title="Curry" image={Curry} parentAction={addCurry} />
      )}
    </div>
  );
};

export default SaucesOptions;
