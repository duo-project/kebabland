import React from "react";
import Card from "../../Card/Card";
import "../Options.scss";
import Viande from "../../../images/viande.png";
import Tofu from "../../../images/tofu.png";

const ViandeOptions = ({ addViande, addTofu, value }) => {
  return (
    <div className="grid-choice-viande">
      {value === "viande" ? (
        <Card
          title="Viande"
          image={Viande}
          parentAction={addViande}
          valid="valid"
        />
      ) : (
        <Card title="Viande" image={Viande} parentAction={addViande} />
      )}
      {value === "tofu" ? (
        <Card title="Tofu" image={Tofu} parentAction={addTofu} valid="valid" />
      ) : (
        <Card title="Tofu" image={Tofu} parentAction={addTofu} />
      )}
    </div>
  );
};

export default ViandeOptions;
