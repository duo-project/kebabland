import React from "react";
import Card from "../../Card/Card";
import "../Options.scss";
import Pain from "../../../images/kebab.png";
import Galette from "../../../images/galette.png";
import Baguette from "../../../images/baguette.png";

const PainOptions = ({ addPain, addBaguette, addGalette, value }) => {
  return (
    <div className="grid-choice">
      {value === "pain" ? (
        <Card title="Pain" image={Pain} parentAction={addPain} valid="valid" />
      ) : (
        <Card title="Pain" image={Pain} parentAction={addPain} />
      )}
      {value === "galette" ? (
        <Card
          title="Galette"
          image={Galette}
          parentAction={addGalette}
          valid="valid"
        />
      ) : (
        <Card title="Galette" image={Galette} parentAction={addGalette} />
      )}
      {value === "baguette" ? (
        <Card
          title="Baguette"
          image={Baguette}
          parentAction={addBaguette}
          valid="valid"
        />
      ) : (
        <Card title="Baguette" image={Baguette} parentAction={addBaguette} />
      )}
    </div>
  );
};

export default PainOptions;
