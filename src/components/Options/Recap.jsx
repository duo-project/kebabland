import React from "react";
import "./Options.scss";

const Recap = ({ children }) => {
  return <div className="recap">{children}</div>;
};

export default Recap;
