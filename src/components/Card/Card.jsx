import React from "react";
import "./Card.scss";

const Card = ({ image, parentAction, title, valid, option2 }) => {
  return (
    <div className={`card ${valid}`} onClick={option2 ? option2 : parentAction}>
      <img src={image} alt="img" />
      <p>{title}</p>
    </div>
  );
};

export default Card;
