import React from "react";
import "./LargeCard.scss";
import Salad from "../../images/salade.png";
import Tomate from "../../images/tomate.png";
import Oignon from "../../images/oignon.png";
import Blanche from "../../images/blache.png";
import Harissa from "../../images/harissa.png";
import Andalouse from "../../images/oignon.png";
import BBQ from "../../images/bbq.png";
import Ketchup from "../../images/ketchup.png";
import Curry from "../../images/curry.png";

const LargeCard = ({ val, parentAction }) => {
  return (
    <>
      <div className="large-card">
        <div className="images">
          {val === "Salade & Tomate & Oignons" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Salad} alt="salade" />
              <img src={Tomate} alt="salade" />
              <img src={Oignon} alt="salade" />
            </div>
          )}
          {val === "Salade & Tomate" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Salad} alt="salade" />
              <img src={Tomate} alt="salade" />
            </div>
          )}
          {val === "Salade & Oignons" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Salad} alt="salade" />
              <img src={Oignon} alt="salade" />
            </div>
          )}
          {val === "Oignons & Tomate" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Oignon} alt="salade" />
              <img src={Tomate} alt="salade" />
            </div>
          )}
          {val === "Oignons" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Oignon} alt="salade" />
            </div>
          )}
          {val === "Tomate" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Tomate} alt="salade" />
            </div>
          )}
          {val === "Salade" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Salad} alt="salade" />
            </div>
          )}
          {/* sauces */}
          {val === "Blanche & Andalouse" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Blanche} alt="salade" />
              <img src={Andalouse} alt="salade" />
            </div>
          )}
          {val === "Blanche & Harissa" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Blanche} alt="salade" />
              <img src={Harissa} alt="salade" />
            </div>
          )}
          {val === "Blanche & BBQ" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Blanche} alt="salade" />
              <img src={BBQ} alt="salade" />
            </div>
          )}
          {val === "Blanche & Ketchup" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Blanche} alt="salade" />
              <img src={Ketchup} alt="salade" />
            </div>
          )}
          {val === "Blanche & Curry" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Blanche} alt="salade" />
              <img src={Curry} alt="salade" />
            </div>
          )}
          {val === "Harissa & Andalouse" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Harissa} alt="salade" />
              <img src={Andalouse} alt="salade" />
            </div>
          )}
          {val === "Harissa & BBQ" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Harissa} alt="salade" />
              <img src={BBQ} alt="salade" />
            </div>
          )}
          {val === "Harissa & Ketchup" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Harissa} alt="salade" />
              <img src={Ketchup} alt="salade" />
            </div>
          )}
          {val === "Harissa & Curry" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Harissa} alt="salade" />
              <img src={Curry} alt="salade" />
            </div>
          )}
          {val === "Andalouse & BBQ" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Andalouse} alt="salade" />
              <img src={BBQ} alt="salade" />
            </div>
          )}
          {val === "Andalouse & Ketchup" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Andalouse} alt="salade" />
              <img src={Ketchup} alt="salade" />
            </div>
          )}
          {val === "Andalouse & Curry" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Andalouse} alt="salade" />
              <img src={Curry} alt="salade" />
            </div>
          )}
          {val === "BBQ & Ketchup" && (
            <div onClick={parentAction} className="flex-img">
              <img src={BBQ} alt="salade" />
              <img src={Ketchup} alt="salade" />
            </div>
          )}
          {val === "BBQ & Curry" && (
            <div onClick={parentAction} className="flex-img">
              <img src={BBQ} alt="salade" />
              <img src={Curry} alt="salade" />
            </div>
          )}
          {val === "Ketchup & Curry" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Ketchup} alt="salade" />
              <img src={Curry} alt="salade" />
            </div>
          )}
          {val === "Blanche" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Blanche} alt="salade" />
            </div>
          )}
          {val === "Harissa" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Harissa} alt="salade" />
            </div>
          )}
          {val === "Andalouse" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Andalouse} alt="salade" />
            </div>
          )}
          {val === "BBQ" && (
            <div onClick={parentAction} className="flex-img">
              <img src={BBQ} alt="salade" />
            </div>
          )}
          {val === "Ketchup" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Ketchup} alt="salade" />
            </div>
          )}
          {val === "Curry" && (
            <div onClick={parentAction} className="flex-img">
              <img src={Curry} alt="salade" />
            </div>
          )}
          {val === "empty" && (
            <div onClick={parentAction} className="flex-img">
              Sauces is
            </div>
          )}
          {val === "empty " && (
            <div onClick={parentAction} className="flex-img">
              Garnitures is
            </div>
          )}
        </div>
        <div className="title-lrcard">{val}</div>
      </div>
    </>
  );
};

export default LargeCard;
