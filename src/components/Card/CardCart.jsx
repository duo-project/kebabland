import React from "react";
import "./CardCart.scss";
import { AiOutlineDelete, AiOutlineMinus, AiOutlinePlus } from "react-icons/ai";

const CardCart = ({
  addQuantity,
  reduceQuantity,
  recap,
  quantity,
  removeKebab,
}) => {
  return (
    <div className={`card-cart`}>
      <p>{recap}</p>
      <div className="quantity">x{quantity}</div>
      <div className="increase-quantity" onClick={addQuantity}>
        <AiOutlinePlus />
      </div>
      <div className="decrease-quantity" onClick={reduceQuantity}>
        <AiOutlineMinus />
      </div>
      <div className="sup-kebab" onClick={removeKebab}>
        <AiOutlineDelete />
      </div>
    </div>
  );
};

export default CardCart;
