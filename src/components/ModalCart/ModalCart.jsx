import React, { useEffect, useState } from "react";
import ButtonCommand from "../Button/ButtonCommand";
import CardCart from "../Card/CardCart";
import "./ModalCart.scss";
import Aos from "aos";
import "aos/dist/aos.css";

const ModalCart = ({ price, commandAction, store, removeKebab }) => {
  const [refresh, setRefresh] = useState(false);
  const addQuantity = (kebab, price) => {
    kebab.quantity = kebab.quantity + 1;
    price = price + 5.5;
    setRefresh(!refresh);
  };
  const reduceQuantity = (kebab, cb) => {
    if (kebab.quantity === 1) {
      cb();
    } else {
      kebab.quantity = kebab.quantity - 1;
      price = price - 5.5;
      setRefresh(!refresh);
    }
  };
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  return (
    <div className="modal-cart" data-aos="fade-left">
      <div className="price">Total : {price} €</div>
      <ButtonCommand parentAction={commandAction} />
      <div className="title-commande">Votre commande</div>
      {store.map((kebab, index) => {
        return (
          <CardCart
            key={index}
            quantity={kebab.quantity}
            recap={`Kebab ${kebab.pain} ${kebab.viande}, ${kebab.garniture}, ${kebab.sauces}`}
            removeKebab={() => {
              removeKebab(kebab);
            }}
            addQuantity={() => addQuantity(kebab, price)}
            reduceQuantity={() =>
              reduceQuantity(kebab, () => removeKebab(kebab))
            }
          />
        );
      })}
    </div>
  );
};

export default ModalCart;
