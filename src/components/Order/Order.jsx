import React from "react";
import "./Order.scss";
import Order_img from "../../images/waiting.png";
import Button from "../Button/Button";

const Order = ({parentAction}) => {
  return (
      <div className="content_page_order" data-aos="zoom-in">
        <h3>C'est Parti !</h3>
        <h4>
          Notre maitre kebabier est entrain de préparer votre commande. <br />
          Patience. . .
        </h4>
        <img src={Order_img} alt="Order img" />
        <Button title='Retour' parentAction={parentAction} />
      </div>
  );
};

export default Order;
