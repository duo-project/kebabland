import React from "react";
import "../Error404/Error404.scss";
import Errorimg from "../../images/errorKebab.png";

function Error404() {
  return (
    <>
      <div className="global_page">
        <div className="content_page">
          <img className="error_img" src={Errorimg} alt="error404" />
          <button className="btn">Back To Menu</button>
        </div>
      </div>
    </>
  );
}

export default Error404;
