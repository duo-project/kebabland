# kebabland

## Installation et lancement du projet
- Déplacez vous dans le dossier du projet
- Installez les modules
- Lancez le projet

```
cd project-folder
npm install
npm run start
```

## Fonctionnement du site

En arrivant sur la homepage, vous pouvez choisir entre 3 kebabs prédéfinis :

- [ ] Le classique
- [ ] Le vege
- [ ] Le bbq

ou la création de votre kebab personnalisé :

- Choisissez vos ingrédients puis cliquez sur "continuer"
- Sur l'écran de récapitulation, vous pouvez cliquer sur les ingrédients pour les changer si besoin
- À droite de l'écran est affiché le panier avec le prix et la composition de vos kebabs.
- Vous pouvez les supprimer de la commande en cas d'erreur.
